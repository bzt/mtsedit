MTSEdit Import File Formats
===========================

This document describes the formats that MTSEdit can import schematics from. For the native format, that can be written too and
which is also used by the Minetest Engine, see the [MTS format](https://gitlab.com/bztsrc/mtsedit/blob/master/docs/mts_format.md).

Blueprint PNG
-------------

MTSEdit is capable to save schematic blueprints to PNG files using the `-b` or `-B` flags. These can be imported too.

Blueprints are simple 32 bit PNG images, which has layers from top to bottom, separated by 1 pixel vertically, starting at the
2nd coloumn. In the first coloumn, there's a black pixel (full opaque black, 0xFF000000) for each layer start. This is used
to detect where the first layer starts and what the layer's height is. If that particular layer is the ground level, then that
pixel is full red (0xFF0000FF). That's all. Layer width ends at the first blue background pixel (0xFF3F0000, which must not be
used in node encodings) or at image width minus 1.

If blueprint is not detected on the image, then the entire image is read as a single layer blueprint finding the blocks that
are closest match in color to each pixel. This feature can be used to voxelize any image up to the size of 256 x 256.

Limitations: rotation info is not stored in blueprint images at all.

WorldEdit Schematics
--------------------

These are [Lua tables](https://wiki.minetest.net/Mods/WorldEdit#Schematics) saved by the *Minetest* [WorldEdit mod](https://content.minetest.net/packages/sfan5/worldedit).
Typical extensions are `.we` or `.we.gz`, `.we.bz2` (no need to decompress, MTSEdit can read compressed versions on-the-fly).

Limitations: none. Everything that can be stored in MTS, can be stored in WorldEdit Schematics as well.

Minecraft Schematic Files
-------------------------

These are [Minecraft NBT](https://minecraft.gamepedia.com/Schematic_file_format) files, used by many Minecraft-compatible
editors. Typical extension is `.schematic`. Only the [Blocks](https://minecraft.gamepedia.com/Java_Edition_data_values/Pre-flattening/Block_IDs)
and Data chunks are parsed, and only used to import schematics. MTSEdit does not save .schematic files.

Limitations: rotation info are not imported properly, but works for basic nodes most of the time (when it is stored in Data
block's least significant 2 bits).

Sponge Schematic Files
----------------------

These are [Minecraft NBT](https://minecraft.gamepedia.com/Schematic_file_format) files too, used by *Minecraft* WorldEdit. Typical
extension is `.schem`, or sometimes `.schematic`. Instead of Blocks and Data chunks, these have [BlockData](https://github.com/SpongePowered/Schematic-Specification/blob/master/versions/schematic-2.md)
and Palette chunks. Although BlockData is a byte array, it contains bigger than 255 values using some messed up "varint" encoding.

Limitations: rotation info are not imported properly, but works for basic nodes most of the time (when it's stored in in the
Palette as "facing=north", "facing="west" etc.).

Model 3D Files
--------------

This is an application independent, universal [Model 3D](https://bztsrc.gitlab.io/model3d) format, capable of storing voxel images.
Typical extension is `.m3d`. More than just an a voxel image, because it can store names and rotation info to voxel types, so it
is perfectly capable to reliably store schematic. If voxel type names are not specified, then it fallbacks to closest color match.

Limitations: none. Everything that can be stored in MTS, can be stored in M3D as well.

Tiled TMX Files
---------------

Produced by the very popular [Tiled](http://www.mapeditor.org). Typical extension is `.tmx`. This format can store schematics
node correctly, but unfortunatelly Tiled is so flexible that any tileset configuration is possible, and neither tile names nor
colors are not stored in the TMX files. So by default, MTSEdit will import these files assuming tile ids being the same as in
Minecraft NBT files. If you have used a different tileset, then you can use the `-r` remap option to specify exactly which tile
numberic id corresponds to which block type name. See [batch mode](https://gitlab.com/bztsrc/mtsedit/blob/master/docs/batch.md)
for more details.

Alternatively you can use the [Minetest_Tiled.tsx](https://gitlab.com/bztsrc/mtsedit/blob/master/etc/Minetest_Tiled.tsx)
external tile set in the etc directory (requires the file etc/Minetest_Tiled.png too), which uses Minecraft NBT IDs, so no
remapping needed with this tile set.

Limitations: only CSV encoded layers are supported for now. without properly remapping the tile numeric ids to your tileset,
you'll get the schematic correctly, but with bad node blocks.

Goxel GOX Files
---------------

These are saved by [Goxel](https://goxel.xyz). Typical extension is `.gox`. [GOX file format](https://github.com/guillaumechereau/goxel/blob/master/src/formats/gox.c)
is a very limited format, but Goxel is a popular voxel editor, so you can import these files (more or less, there has to be some
fixups before you can use these as proper MTS files). In lack of better alternative, nodes are matched by closest color only. This
means that nodes can't be imported reliably, they are usually identified correctly, but not all the time. To help you out with
this, you can find a [GIMP Palette](https://gitlab.com/bztsrc/mtsedit/blob/master/etc/Mineclone2.gpl) in the etc directory, which
Goxel can use. Using only colors from that palette in Goxel will provide you exact color matches on GOX file imports. If you
change the blocks.csv or the block images, then you can regenerate that palette with the `-G` command line option.

Limitations: this format can't store nodes, nor rotation info, only colorized voxels. This is a voxel image format, not a true
schematic format.

Magicavoxel VOX Files
---------------------

Used by [Magicavoxel](https://ephtracy.github.io). Typical extension is `.vox`. Like Goxel files the
[VOX file format](https://github.com/ephtracy/voxel-model/blob/master/MagicaVoxel-file-format-vox.txt) stores voxel images, so
the same limitations apply. They are not perticularly good for storing schematics, as nodes has to be matched by color some nodes
might be incorrectly identified.

To aid workflow in both ways, as an exception MTSEdit can convert to VOX files too. This is the only format that's written which
is not MTS.

Limitations: this format can't store nodes, nor rotation info, only colorized voxels. This is a voxel image format, not a true
schematic format.

Qubicle QB Files
----------------

Saved by the proprietary, but popular [Qubicle](https://getqubicle.com) editor. Typical extension is `.qb`. The
[QB file format](https://getqubicle.com/learn/article.php?id=22) has exactly the same limitations as the previous two, it is not
a schematic format, just a voxel image format. Nodes are identified by closest color match, some might be incorrectly set.

MTSEdit otherwise has full QB support, all variants supported: compressed / uncompressed, RGBA / ARGB, visbility mask,
left-handed / right-handed coordinate systems etc.

Limitations: this format can't store nodes, nor rotation info, only colorized voxels. This is a voxel image format, not a true
schematic format.

