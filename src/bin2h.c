/*
 * mtsedit/bin2h.c
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Small utility to convert binary to C header, because MacOSX does not support ld -b binary!
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <stdint.h>
#ifndef _MSC_VER
#ifndef _inline
#define _inline __inline__
#endif
#ifndef _pack
#define _pack __attribute__((packed))
#endif
#ifndef _unused
#define _unused __attribute__((unused))
#endif
#else
#define _inline
#define _pack
#define _unused
#endif
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

char tmp[1024];

unsigned char *getdir(char *path, char *prefix, unsigned char *buff, long *size)
{
    FILE *f;
    DIR *dir, *dir2;
    struct dirent *de;
    char *fn;
    int i;
    long s;

    if(path != tmp) strcpy(tmp, path);
    i = strlen(tmp);
    if(tmp[i-1] != '/') { tmp[i++] = '/'; tmp[i] = 0; }
    fn = &tmp[i];
    if(!prefix) prefix = &tmp[i];
    dir = opendir(path);
    if(dir) {
        while((de = readdir(dir))) {
            i = strlen(de->d_name);
            if(de->d_name[0] == '.') continue;
            strcpy(fn, de->d_name);
            dir2 = opendir(tmp);
            if(dir2) {
                closedir(dir2);
                buff = getdir(tmp, prefix, buff, size);
            } else {
                f = fopen(tmp,"rb");
                if(f) {
                    fseek(f, 0, SEEK_END);
                    s = ftell(f);
                    fseek(f, 0, SEEK_SET);
                    i = strlen(prefix) + 1;
                    buff = (unsigned char*)realloc(buff, *size + i + 2 + s);
                    if(!buff) {
                        fprintf(stderr, "memory allocation error +%d\r\n", i);
                        exit(2);
                    }
                    memcpy(buff + *size, prefix, i);
                    *size += i;
                    memcpy(buff + *size, &s, 2);
                    *size += 2;
                    fread(buff + *size, s, 1, f);
                    *size += s;
                    fclose(f);
                }
            }
        }
        closedir(dir);
    }
    return buff;
}

int main(int argc, char **argv)
{
    FILE *f;
    long size;
    unsigned char *buff = NULL, *buff2 = NULL;
    char *fn, name[255];
    int i, file;

    if(argc < 2){
        printf("bin2h <bin file> [bin file2...]\r\n");
        exit(1);
    }

    for(file = 1; file < argc; file++){
        size = 0;
        buff = getdir(argv[file], NULL, NULL, &size);
        if(buff) {
            buff2 = stbi_zlib_compress(buff, (int)size, &i, 9);
            if(buff2) { free(buff); buff = buff2; size = i; }
        } else {
            f = fopen(argv[file],"rb");
            if(f) {
                fseek(f, 0, SEEK_END);
                size = ftell(f);
                fseek(f, 0, SEEK_SET);
                buff = (unsigned char*)malloc(size);
                if(!buff) {
                    fprintf(stderr, "memory allocation error\r\n");
                    exit(2);
                }
                fread(buff, size, 1, f);
                fclose(f);
            }
        }
        if(buff) {
            fn = strrchr(argv[file], '/');
            if(!fn) fn = strrchr(argv[file], '\\');
            if(!fn) fn = argv[file]; else fn++;
            for(i = 0; fn[i]; i++)
                name[i] = fn[i] == '.' || fn[i] <= ' ' ? '_' : fn[i];
            name[i] = 0;
            printf("unsigned char binary_%s[%ld] = { ", name, size);
            for(i = 0; i < size; i++) {
                if(i) printf(", ");
                printf("0x%02x", buff[i]);
            }
            printf(" };\n");
            free(buff);
            buff = NULL;
        } else
            fprintf(stderr, "Unable to open input file: %s\n", argv[file]);
    }
}
