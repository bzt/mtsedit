/*
 * mtsedit/load.c
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Load file window
 *
 */

#include "main.h"
#include <unistd.h>

int loadfld = 0, loadmax = 0, loadpos = 0, loadscr = 0;
char **loadfiles = NULL;
extern int savepos, savelen;
extern char cur[2];

/**
 * Do the file load
 */
void load_do()
{
    char *fn, d = loadfiles[loadpos][0] == '/';

    if(isdir(mtsfile)) {
        fn = mtsfile + strlen(mtsfile);
        if(fn[-1] != DIRSEP) { *fn++ = DIRSEP; *fn = 0; }
    } else {
        fn = strrchr(mtsfile, DIRSEP);
        if(!fn) fn = mtsfile; else fn++;
        *fn = 0;
    }
    if(!mtsfile[0] || (mtsfile[0] == '.' && (!mtsfile[1] || (mtsfile[1] == DIRSEP && !mtsfile[2])))) {
        getcwd(mtsfile, MAXPATHLEN);
        fn = mtsfile + strlen(mtsfile);
        if(fn[-1] != DIRSEP) { *fn++ = DIRSEP; *fn = 0; }
    }
    if(!strcmp(loadfiles[loadpos], "/..")) {
        fn[-1] = 0;
        fn = strrchr(mtsfile, DIRSEP);
        if(!fn) fn = mtsfile; else fn++;
        *fn = 0;
        if(!mtsfile[0]) {
            getcwd(mtsfile, MAXPATHLEN);
            fn = mtsfile + strlen(mtsfile);
            if(fn[-1] != DIRSEP) { *fn++ = DIRSEP; *fn = 0; }
        }
    } else
        strcpy(fn, loadfiles[loadpos] + d);
    freedir(loadmax, &loadfiles);
    if(d) loadpos = loadscr = loadmax = 0;
    savelen = savepos = strlen(mtsfile);
    if(!isdir(mtsfile))
        sdldo(-2);
}

/**
 * Redraw the Load file window
 */
void load_redraw()
{
    int i, j, k;
    SDL_Rect rect;

    rect.x = 36; rect.y = 0; rect.w = screen->w - 36; rect.h = screen->h - font->height;
    SDL_FillRect(screen, &rect, theme[THEME_BG]);

    strmaxw = screen->w - 5;
    sdlprint((screen->w - 47 - mbstrlen(lang[LOADFROM]) * (font->width+1)) / 2 + 47, 4, THEME_INPUT, THEME_BG, lang[LOADFROM]);
    rect.y = 12 + font->height;

    sdlprint(42, rect.y, THEME_FG, THEME_BG, lang[FILENAME]);
    rect.x = 200; rect.w = screen->w - 205; rect.h = font->height + 2;
    SDL_FillRect(screen, &rect, theme[THEME_INPBG]);
    strmaxw = screen->w - 5;
    sdlprint(201,rect.y + 1, !loadfld ? THEME_INPUT : THEME_INACT, THEME_INPBG, mtsfile);
    if(!loadfld) {
        cur[0] = mtsfile[savepos] ? mtsfile[savepos] : ' ';
        sdlprint(201 + savepos * (font->width+1), rect.y + 1, THEME_INPBG, THEME_INPUT, cur);
    }
    rect.y = 16 + 2*font->height;

    j = screen->h - 2*font->height - 8;
    rect.x = 36; rect.h = j - rect.y; rect.w = screen->w - 41;
    SDL_FillRect(screen, &rect, theme[THEME_INPBG]);

    if(!loadfiles)
        loadmax = listdir(mtsfile, &loadfiles, 1);
    for(i = loadscr; i < loadmax && (rect.y + (int)font->height) < j; i++, rect.y += font->height) {
        if(loadfiles[i][0] == '/') {
            strsepar = 0;
            sdlprint(44, rect.y, loadfld == 1 && loadpos == i ? THEME_INPUT : THEME_INACT, THEME_INPBG, "\002\003");
            strsepar = 1;
            k = 1;
        } else k = 0;
        sdlprint(48 + 2*font->width, rect.y, loadfld == 1 && loadpos == i ? THEME_INPUT : THEME_INACT, THEME_INPBG,
            loadfiles[i] + k);
    }

    rect.y = j + 4;
    rect.h = font->height + 2;
    SDL_FillRect(screen, &rect, theme[loadfld == 2 ? THEME_INPUT : THEME_FG]);
    sdlprint((screen->w - 47 - mbstrlen(lang[LOADBTN]) * (font->width+1)) / 2 + 42, rect.y+1,
        THEME_BG, loadfld == 2 ? THEME_INPUT : THEME_FG, lang[LOADBTN]);
}

/**
 * Load file window scrolling event handler
 */
void load_scroll(SDL_Event *event)
{
    loadscr -= event->wheel.y;
    if(loadscr < 0) loadscr = 0;
    if(loadscr + 1 >= loadmax) loadscr = loadmax - 1;
}

/**
 * Load file window mouse down event handler
 */
void load_mousedown(SDL_Event *event)
{
    int i = screen->h - 2*font->height - 4;
    if(event->button.x < 42) return;
    if(event->button.y >= (int)(12 + font->height) && event->button.y < (int)(16 + 2*font->height) && loadfld) {
        loadfld = 0; savepos = savelen = strlen(mtsfile);
    }
    if(event->button.y >= i && event->button.y < (int)(i + font->height)) load_do();
    else if(event->button.y >= (int)(16 + 2*font->height) && event->button.y < i) {
        i = loadscr + (event->button.y - (16 + 2*font->height)) / font->height;
        if(i + 1 >= loadmax) i = loadmax - 1;
        if(loadfld != 1) {
            loadpos = i;
        } else {
            if(i != loadpos) loadpos = i;
            else load_do();
        }
        loadfld = 1;
    }
}

/**
 * Load file window key event handler
 */
void load_key(SDL_Event *event)
{
    int i, j;

    switch(event->type) {
        case SDL_TEXTINPUT:
            j = strlen(event->text.text);
            if(!loadfld && savelen + j < (int)(sizeof(mtsfile))) {
                for(i = savelen - 1; i >= savepos; i--) mtsfile[i + j] = mtsfile[i];
                memcpy(mtsfile + savepos, &event->text.text, j);
                savepos += j;
                savelen += j;
                mtsfile[savelen] = 0;
                freedir(loadmax, &loadfiles);
            }
        break;
        case SDL_KEYUP:
            switch (event->key.keysym.sym) {
                case SDLK_TAB:
                    if(loadfld >= 2) loadfld = 0; else loadfld++;
                break;
                case SDLK_BACKSPACE:
                    if(!loadfld && savepos) {
                        savepos--;
                        for(i = savepos; i < savelen + 1; i++) mtsfile[i] = mtsfile[i+1];
                        savelen--;
                        freedir(loadmax, &loadfiles);
                    }
                break;
                case SDLK_DELETE:
                    if(!loadfld && savepos < savelen) {
                        for(i = savepos; i < savelen + 1; i++) mtsfile[i] = mtsfile[i+1];
                        savelen--;
                        freedir(loadmax, &loadfiles);
                    }
                break;
                case SDLK_UP:
                    if(!loadfld) savepos = 0;
                    if(loadfld == 1) {
                        if(!loadpos && loadmax) loadpos = loadmax - 1; else loadpos--;
                        if(loadpos < loadscr) loadscr = loadpos;
                        i = ((screen->h - 2*font->height - 8) - (16 + 2*font->height)) / font->height;
                        if(loadpos >= loadscr + i) loadscr = loadpos - i;
                    }
                break;
                case SDLK_DOWN:
                    if(!loadfld) savepos = savelen;
                    if(loadfld == 1) {
                        if(loadpos + 1 >= loadmax) loadpos = 0; else loadpos++;
                        if(loadpos < loadscr) loadscr = loadpos;
                        i = ((screen->h - 2*font->height - 8) - (16 + 4*font->height)) / font->height;
                        if(loadpos >= loadscr + i) loadscr = loadpos - i;
                    }
                break;
                case SDLK_LEFT:
                    if(!loadfld && savepos) savepos--;
                break;
                case SDLK_RIGHT:
                    if(!loadfld && savepos < savelen) savepos++;
                break;
                case SDLK_RETURN: load_do(); break;
            }
        break;
    }
}
