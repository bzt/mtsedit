# textdomain: mtsedit
Specify <palette> (like "Mineclone2", "Minetest Game" etc.)=Especifique <paleta> (como "Mineclone2", "Minetest Game" etc.)
Unable to write @1=Não foi possível escrever @1
@1 saved.=@1 salvo.
Specify <schematic> file=Especifique o arquivo <esquema>
Failed to load @1=Falha ao carregar @1
Schematic loaded, use right click with the placement tool in your inventory=Esquema carregado, use o botão direito do mouse com a ferramenta de veiculação em seu inventário
Select an area first by left clicking with the placement tool=Selecione uma área primeiro clicando com o botão esquerdo da ferramenta de posicionamento
Area=Área
Specify <delta> value=Especifique o valor <delta>
Place the loaded structure=Coloque a estrutura carregada
Failed to place the schematic @1=Falha ao colocar o esquema @1
First use the "/mtsedit load" chat command to load a schematic=Primeiro, use o comando de bate-papo "/mtsedit load" para carregar um esquema
Load and save MTS files easily or export MTSEdit block data=Carregue e salve arquivos MTS facilmente ou exporte dados do bloco MTSEdit
[load|list|save|export|imgs|help] [file]=[load|list|save|export|imgs|help] [arquivo]
Load an MTS file from "(world directory)/schems/<schematic>.mts" into the world=Carregue um arquivo MTS de "(diretório mundial)/schems/<esquema>.mts" no mundo
Show available schematic files=Mostrar arquivos esquemáticos disponíveis
Save the selected area into an MTS file=Salve a área selecionada em um arquivo MTS
Grow or shrink the selected area=Aumente ou diminua a área selecionada
Export node data to <palette> column of "(world directory)/blocks.csv"=Exportar dados do nó para a coluna <paleta> de "(diretório mundial)/blocks.csv"
Export node texture data to "(world directory)/blockimgs.csv"=Exportar dados de textura do nó para "(diretório mundial)/blockimgs.csv"
Unknown mtsedit command=Comando mtsedit desconhecido
