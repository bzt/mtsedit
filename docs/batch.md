MTSEdit User Manual
===================

This is the description of the windowless operation mode.

See also the [GUI usage](https://gitlab.com/bztsrc/mtsedit/blob/master/docs/usage.md).

```
MineTest Schematics Editor by bzt Copyright (C) 2019 MIT license

./mtsedit [-h] [-v] [-l lang] [-d|-p|-P|-b|-B|-V] [-r remapfile] [-m map] <.mts|.schematic|.tmx|.gox|.vox|.qb> [out.mts]
./mtsedit [-G|-C|-T]
./mtsedit -g <block.png>
./mtsedit -t <blockimgs.csv> [blockid]
./mtsedit -i [Minetest mods dir]

  -v: verbose output
  -l lang: set language
  -d: dump layers to output
  -p: save preview
  -P: save preview, cut structure in half
  -b, -B: save blueprint
  -r remapfile: remap numeric ids in input
  -m map: replace block type mapping
  -g: generate slab and stairs from block image
  -t: generate block images from texture data
  -i: install Minetest mod
  -V: output to VOX
  out.mts: output to file
```

Batch Mode
----------

You can use this tool to bulk process schematic files from scripts.

### Dump Schematic to Standard Output

This will print out mapping and each block as hex values layer by layer.
```
./mtsedit -d structure.mts
```
Also works with all the supported [input formats](https://gitlab.com/bztsrc/mtsedit/blob/master/docs/import.md), for example:
```
./mtsedit -d structure.schem
```

### Generate BluePrint

Pretty much the same as dump, but instead of dumping the structure to the console, it saves "structure_bp.png".
```
./mtsedit -b structure.mts
```
Each layer will be padded by 1 x 1 pixels of transparency. On the left, there's a black pixel at each layer start so
that programs can detect the dimensions from the image. On the right, there's a red line for the ground layer.

The `-b` flag will also save a blue background and block legend on the image. `-B` only saves the raw blueprint. MTSEdit
doesn't care, it can import both blueprint types.

### Conversion (or Remap Output)

You can convert [different formats](https://gitlab.com/bztsrc/mtsedit/blob/master/docs/import.md) like Minecraft NBT Schematics
and Goxel and Magicavoxel voxel images with this tool.
```
./mtsedit from.schematic structure.mts
```

You can also convert MTS files using different block type mappings:

```
./mtsedit -m "Minetest Game" mcl2.mts mtg.mts
```
Where the parameter to "-m" is a coloumn name in [blocks.csv](https://gitlab.com/bztsrc/mtsedit/blob/master/docs/blocks.md) file,
from the 6th coloumn and upwards. You can also use numbers, starting from 1 (which means the 6th coloumn, as the first five are
reserved for special use).
```
./mtsedit -m 1 mtg.mts mcl2.mts
```
This will save the block name mapping in that coloumn to the output file.

### Remap Block IDs in Input Files

Some input formats (.schematic, .tmx etc.) does not contain names at all, just numeric ids. For those formats by default the
Minecraft NBT IDs are assumed (0 = Air, 1 = Stone Block, 2 = Grass Block etc.), specified in
[blocks.csv](https://gitlab.com/bztsrc/mtsedit/blob/master/docs/blocks.md)'s 2nd coloumn. However this is not always the case, so
remapping of numeric ids in input files may be needed, and that's what `-r` is for (in contrast to `-m`, which selects the mapping
for the output file). This flag expects a filename as a parameter, which is a simple newline '\\n' terminated text file. The use of
carrige return '\\r' is optional. Each line contains exactly one block id (canonical block type name, 1st coloumn in blocks.csv),
and the line number corresponds to the numeric id minus one. So for example the canonical name of numeric id 0 is in the first
line, name for the numeric id 1 is in the second line, name for the numeric id 2 is in the third line etc. For the unset (or empty)
tile, use the name "Air".

```
./mtsedit -r minetest_tiled.txt schematic.tmx mtg.mts
```

If you have used Minecraft NBT IDs in the input file, then remapping is unnecessary.

### Converting to VOX

Normally convertion creates only MTS files. But to support bidirectional workflows, MTSEdit can write VOX files too using `-V`.
```
./mtsedit -V structure.mts structure.vox
```
Note that VOX files will loose a lot of schematic's details (node names, rotation info, probabilities, force placement etc.), but
at least you will be able load the structure in Goxel and Magicavoxel. When saved, you can convert VOX (or GOX) files back to MTS:
```
./mtsedit structure.vox structure.mts
```

### Generate Preview

```
./mtsedit -p somedir/structure.mts
```
This will save a file by the same name as the MTS but with ".png" extension, like "somedir/structure.png".

```
./mtsedit -P somedir/structure.mts
```
With uppercase "P", you can cut the structure in half to peek inside.

### Generate Pictures for Slab and Stairs

This wasn't a planned feature, but I needed quick and dirty tool to generate differently rotated and cut block images from
a cube block node image.

```
./mtsedit -g Gold_Block.png
```
This will save "Gold_Block_Slab.png", "Double_Gold_Block_Slab.png" and "Gold_Block_Stair.png". Also saves a mirrored image added
to "Gold_Block.png" if it's height is 32. Mirroring does not look good because of the wrong direction of the shadow, but better
than nothing for nodes like furnace. For a proper solution, generate block images from textures.

### Generate Block Images from Textures

This one was intended. With the [MTSEdit Minetest Mod](https://gitlab.com/bztsrc/mtsedit/tree/master/mt-mod) you can save
`blockimgs.csv` with the node's texture data. To import that data, use
```
./mtsedit -t ~/.minetest/worlds/MyWorld/blockimgs.csv
```
This will generate the block images as well as the slab and stair images into the CURRENT WORKING directory. You can then select
what you need and copy those to the `data` directory on your system. I would suggest to run this command in a new, empty directory,
because it will generate a lot of PNGs. And by a lot, I mean A LOT (in magnitude of thousands probably). Alternatively you can
specify a block id (canonical name) and only that particular block's images will be generated:
```
./mtsedit -t ~/.minetest/worlds/MyWorld/blockimgs.csv Crafting_Table
```
If you specify a cube block node (drawing type normal) then its slab, double slab and stair variants will be generated too if they
are defined in the blockimgs.csv. There's no need to call `-t` with "Cobblestone_Stair" for example, just with "Cobblestone".
In order to work, the block types must be named like this: ends with "\_Slab"; starts with "Double\_" and ends with "\_Slab"; or
ends with "\_Stair".

### Generate GIMP Palette for nodes

Some voxel editors (like Goxel) can use GIMP palettes. You can generate this with the `-G` flag.

```
./mtsedit -G >mypalette.gpl
```

Because GPL can also store color names in the color file, you can use `-m` to select a specific mapping:
```
./mtsedit -m "Mineclone2" -G >mcl2_colors.gpl
```

### Generate Minetestmapper Palette

[Minetestmapper](https://github.com/Rogier-5/minetest-mapper-cpp) is a neat little tool to generate a map of a world. For that
it requires a palette with node names and RGB codes. You can generate this with the `-C` flag.

```
./mtsedit -C >colors.txt
```
Because Minetestmapper also needs node names in the color file, you can use `-m` to select a specific mapping:
```
./mtsedit -m "Mineclone2" -C >mcl2_colors.txt
```
